var path = require('path');
var fs = require('fs');
var express = require('express');

// Server
var app = express();
app.use(express.static(__dirname + '/dist'));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/dist/index.html'));
});

var server = app.listen(8088);
console.log('Server listening on port 8088');

// Socket
var io = require('socket.io')(server);
io.binaryType = 'arraybuffer';


io.on('connection', function (socket) {
    console.log('New client connected!');

    socket.on('add_comment', function () {
        console.log('dasd')
        io.emit('new_comment', new ArrayBuffer);
    });

});